@echo off

echo ## %1 ##
echo ## fetching %1
git fetch
echo ## Adding remote [name=tp] ssh://git@bitbucket.org/touchedprojects/%1.git
git remote add tp ssh://git@bitbucket.org/touchedprojects/%1.git
echo ## Pushing branches to %1
git push tp refs/remotes/origin/*:refs/heads/*
echo ## Pushing tags to %1
git push tp --tag
echo ## Removing remote [name=tp] for %1
git remote remove tp
pause
