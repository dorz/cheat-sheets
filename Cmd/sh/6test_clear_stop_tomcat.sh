#!/bin/bash

echo "ZATRZYMYWANIE PRACY TOMCATA..."
sh /wwwapp/apache-tomcat-6.0.37-test/bin/shutdown.sh

echo "USUWANIE ROZPAKOWANYCH DANYCH DOCUSAFEA..."
rm -rf /wwwapp/apache-tomcat-6.0.37-test/webapps/docusafe/
rm -rf /wwwapp/apache-tomcat-6.0.37-test/webapps/docusafetest/

echo "CZYSZCZENIE WORKA..."
rm -rf /wwwapp/apache-tomcat-6.0.37-test/work/Catalina/

echo "ZABIJANIE PROCESU..."
pkill -f /wwwapp/apache-tomcat-6.0.37-test/
