#!/bin/bash

echo "ZATRZYMYWANIE PRACY TOMCATA..."
sh /wwwapp/apache-tomcat-7.0.54/bin/shutdown.sh

echo "ZABIJANIE PROCESU..."
pkill -f /wwwapp/apache-tomcat-7.0.54/

echo "URUCHAMIANIE TOMCATA..."
sh /wwwapp/apache-tomcat-7.0.54/bin/startup.sh