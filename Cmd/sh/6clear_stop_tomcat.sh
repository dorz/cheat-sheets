#!/bin/bash

echo "ZATRZYMYWANIE PRACY TOMCATA..."
sh /wwwapp/apache-tomcat-6.0.37/bin/shutdown.sh

echo "USUWANIE ROZPAKOWANYCH DANYCH DOCUSAFEA..."
rm -rf /wwwapp/apache-tomcat-6.0.37/webapps/docusafe/
rm -rf /wwwapp/apache-tomcat-6.0.37/webapps/docusafetest/

echo "CZYSZCZENIE WORKA..."
rm -rf /wwwapp/apache-tomcat-6.0.37/work/Catalina/

echo "ZABIJANIE PROCESU..."
pkill -f /wwwapp/apache-tomcat-6.0.37/
