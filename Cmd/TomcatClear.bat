@echo off

call SetPaths.bat

:VARSION_CHOISE
SET /P tomcatVer=Clear which version of tomcat [6,7,8]:
IF "%tomcatVer%" == "6" GOTO TOMCAT_6
IF "%tomcatVer%" == "7" GOTO TOMCAT_7
IF "%tomcatVer%" == "8" GOTO TOMCAT_8
GOTO VARSION_CHOISE

:TOMCAT_6
SET pathToClear=%tomcat6Path%
goto CLEAR_TOMCAT

:TOMCAT_7
SET pathToClear=%tomcat7Path%
goto CLEAR_TOMCAT


:TOMCAT_8
SET pathToClear=%tomcat8Path%
goto CLEAR_TOMCAT

:CLEAR_TOMCAT
echo Start to clear tomcat in %pathToClear%
echo Start to delete docusafe
rmdir %pathToClear%\webapps\docusafe/s /q
echo Start to delete work directory
rmdir %pathToClear%\work/s /q
echo Start to delete temp directory
rmdir %pathToClear%\temp/s /q

echo Finished to clear tomcat in %pathToClear% 
SET /P isContinue=Do you want to start tomcat %tomcatVer% (y to accept):

IF "%isContinue%" == "y" (call TomcatStart.bat %tomcatVer%)
