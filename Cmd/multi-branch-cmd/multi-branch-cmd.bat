@echo off
title Multi Branch Command Processing
echo.
echo Starting Multi Branch Command Processing...
echo.
echo To stop, press Ctrl+c

setlocal
set CALL_HOME=%cd%
set MBCP_HOME=%~dp0

IF "%1" == "" (GOTO REPO_CHOISE)
:PATH_FROM_PARAMETER
echo setting repository as %1
SET repoPath=%1
GOTO REPO_ROUTER

:REPO_CHOISE
SET /P repoPath=Provide path of repository:

:REPO_ROUTER
IF "%repoPath%" == "" GOTO REPO_CHOISE
cd %repoPath%
echo %cd%
GOTO BRANCH_ITER

:BRANCH_ITER
for /F "tokens=*" %%A in (%MBCP_HOME%\branches.txt) do call :process %%A
goto :END

:process
ECHO Checkout to %1
call git checkout %1
timeout 4 > NUL

echo %cd%
echo Processing branch %1 ...
call %MBCP_HOME%\commands.bat %1

call git checkout master
timeout 4 > NUL
REM echo deleting branch %1 
REM call git branch -d %1
exit /b

:END
echo M.B.C.P. Ko�czy prac�
cd %CALL_HOME%