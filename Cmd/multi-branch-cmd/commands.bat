@echo off
echo Current branch %1
echo %1 : copy .gitignore
cat C:\projects\tmp\.gitignore > .gitignore
echo %1 : commit .gitignore
git add --all
git commit -m "DSRDF-14 Add gitignore"
echo %1 : copy ivy-settings
cat C:\projects\tmp\ivy-settings.xml > ivy-settings.xml
echo %1 : commit ivy-settings
git add --all
git commit -m "DSRDF-15 Fix ivy-settings"
echo ----BRANCH STATUS----
git status
echo ---- Processing %1 Finished -----
REM echo push all changes
REM git push


