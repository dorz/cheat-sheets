@echo off

call SetPaths.bat run

IF "%1" == "" (GOTO VARSION_CHOISE)
:VERSION_FROM_PARAMETER
SET tomcatVer=%1
GOTO VERSION_ROUTER

:VARSION_CHOISE
SET /P tomcatVer=Start which version of tomcat [6,7,8]:

:VERSION_ROUTER
IF "%tomcatVer%" == "6" GOTO TOMCAT_6
IF "%tomcatVer%" == "7" GOTO TOMCAT_7
IF "%tomcatVer%" == "8" GOTO TOMCAT_8
GOTO VARSION_CHOISE

:TOMCAT_6
SET pathToTomcat=%tomcat6Path%
SET pathOut=%tomcat6out%
goto START_TOMCAT

:TOMCAT_7
SET pathToTomcat=%tomcat7Path%
SET pathOut=%tomcat7out%
goto START_TOMCAT


:TOMCAT_8
SET pathToTomcat=%tomcat8Path%
SET pathOut=%tomcat8out%
goto START_TOMCAT

:START_TOMCAT
echo Output should be writen in %pathOut%\TomcatOut.txt

cd %pathToTomcat%\bin
echo Localization of Tomcat to start: %cd%

echo Start Tomcat %tomcatVer% 

.\startup.bat >%pathOut%\TomcatOut.txt 2>1



