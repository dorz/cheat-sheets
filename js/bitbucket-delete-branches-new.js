(function(){

    window.deleteBranches = handleBranchesList;

    /**
     * Here goes code for handling one branch
     */
    function onHandleNextBranch(name){
        console.log("DELETE ", branch);
        $.ajax({
            type:"DELETE",
            url: "https://bitbucket.org/!api/1.0/repositories/compan/docusafe-ilpl/_branch/"+branch,
            success:log
        });
    }


    function handleBranchesList(branches){
        var handler;
        if(_.isString(branches)){
            handler = new BranchesHandler(Array.prototype.slice.call(arguments));
        }else if(_.isArray(branches)){
            handler = new BranchesHandler(branches);
        }else{
            handler = defaultHandler(arguments);
        }
        handler.handle();
    }

    function defaultHandler(args){
        return {
            handle : function() {
                console.error("Illegal Arguments Exception", args)
            }
        }
    }

    function BranchesHandler(branches){
        var originalBranchList = branches || [];
        var branchQueue = originalBranchList.slice();

        this.handle = handle;

        function handle(){
            handeOneBranch(nextBranch());
        }

        function handleOneBranch(name){
            if(!name){
                return;
            }

            console.log("Start handling for branch ", name);
            setTimeout(function(){

                onHandleNextBranch(name);

                handleOneBranch(nextBranch());
                console.log("Done handling for branch ", name);
            },1000);
        }


        function nextBranch() {
            return branchQueue.shift();
        }
    }

})();