module.exports = function(conf){
	const _this = this;
	const msg = conf.message;
	_this.get = function(){
		if(!msg){
			console.error("No message provided in configuration (property 'message')");
			throw "Invalid Configuration (no 'message' property)"
		}
		return msg	
	};
	
	return _this;
}