const https = require('https');
const querystring = require('querystring');
const conf = require('./conf.json');

new HipChatKicker().start();




function HipChatKicker(){
	const _this = this;
	_this.start = start;
	_this.stop = stop;
	const messageProvider = loadMessageProvider();
	var state = new KickerStopped();
	
	
	function start(){
		state = state.start(messageProvider);
	}
	
	function stop(){
		state = state.stop();
	}
	
	function loadMessageProvider(){
		var providerName = conf.messageProvider || "static"
		providerName = "./" + providerName;
		return require(providerName)(conf);
	}
	
}


function KickerStarted(intervalId){
	var _this = this;
	_this.start = start;
	_this.stop = stop;
	
	function stop(){
		clearInterval(intervalId);
		return new KickerStopped();
	} 
	
	function start(){
		console.log("Nothing to start (allready started)");
		return _this;
	}
	
}

function KickerStopped(){
	var _this = this;
	_this.start = start;
	_this.stop = stop;
	
	function start(messageProvider){
		var sender = new HipchatSender(messageProvider);
		var intervalId = setInterval(sender.send,interval());
		return new KickerStarted(intervalId);
	}
	
	function stop(){
		console.log("Nothing to stop (allready stopped)");
		return _this;
	}
	
	function interval(){
		return conf.interval || 1000;
	}
}

function HipchatSender(messageProvider){
	const _this = this;
	const settings = prepareSettings();
	_this.send = send;
	
	function send(){
		var data = prepareData();
		var req = request();
		postDataOnRequest(data,req);
	}
	
	
	
	function prepareSettings(){
		return  {
			method: "POST",
			hostname: "docusafe.hipchat.com",
			path: "/v2/user/"+conf.target+"/message?auth_token="+conf.token,
			headers: {
				"Content-Type" : 'application/json'
			}
		};
	}
	
	function prepareData(){
		return querystring.stringify({
                "message":messageProvider.get(),
                "message_format":"text",
                "notify" : "true"
            });
	}
	
	function request(){
		return https.request(settings, function(response) {
			response.setEncoding('utf8');
			response
			.on('data', success)
			.on('error', error)
		});
		
		function success(data) {
			console.log('Response: ' + data);
		}
		
		function error(data){
			console.warn("Server error" + data);
		}
	}
	
	function postDataOnRequest(data, req){
		req.write(data);
		req.end();
	}
}