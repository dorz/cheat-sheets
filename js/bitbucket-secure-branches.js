/**
 * <p>
 * Add branches to lists:
 * -"Prevent deletion of these branches"
 * -"Prevent history re-writes (rebase) on these branches"
 *
 * Usage:
 * - open https://bitbucket.org/compan/{project-name}/admin/branches
 * - run this script in console
 * - call secureBranches(args) providing as args
 *      * array of branches names ex: secureBranches(["master","test1","develop"])
 *      * branches names separated by comas ex: secureBranches("master","test1","develop")
 * </p>
 */
(function(){

    window.secureBranches = secureBranches;

    function secureBranches(branches){
        var protector;
        if(_.isString(branches)){
           protector = new BranchesProtector(Array.prototype.slice.call(arguments));
        }else if(_.isArray(branches)){
           protector = new BranchesProtector(branches);
        }else{
            protector = defaultProtector(arguments);
        }
        protector.startProtection();
    }

    function defaultProtector(args){
        return {
            startProtection : function() {
                console.error("Illegal Arguments Exception", args)
            }
        }
    }

    function BranchesProtector(branches){
        var originalBranchList = branches || [];
        var branchQueue = originalBranchList.slice();

        this.startProtection = startProtection;

        function startProtection(){
            secureBranch(nextBranch());
        }

        function secureBranch(name){
            if(!name){
                return;
            }

            console.log("Start to protect branch ", name);
            setTimeout(function(){
                console.log("Applying delete protection on branch ", name);
                preventFromDeletion(name);
                console.log("Applying rewrites protection on branch ", name);
                preventFromRewrites(name);

                secureBranch(nextBranch());
                console.log("Protection applied for branch ", name);
            },500);
        }


        function preventFromDeletion(branchName){
            submitBranchPreventing(branchName, 'delete');
        }
        function preventFromRewrites(branchName) {
            submitBranchPreventing(branchName, 'force');
        }

        function submitBranchPreventing(branchName, action) {
            var branchNameInput = $('#pattern_input_' + action);
            branchNameInput.val(branchName);
            branchNameInput.siblings("button").disable(false).click();
        }

        function nextBranch() {
            return branchQueue.shift();
        }
    }

})();