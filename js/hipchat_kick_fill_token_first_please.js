(function (){
    var auth_token = "";

    var _this = this;

    var kickerId = null;

    _this.counter = 0;
    _this.message = "!";
    _this.multiple = true;

    _this._startKicking = startKicking;
    _this._stopKicking = stopKicking;
	_this._oneKick = kickUser;

    function startKicking(userName, message, multiple, interval){
        if(kickerId != null){
            throw "can kick one user at once"
        }

        if(userName == null){
            throw "userName could not be null"
        }
        _this.userName = userName;

        _this.message = message || _this.message;

        if(multiple != null){
			
            _this.multiple = multiple;
        }
        interval < 1000 ? interval = 1000 : interval;

		_this.counter = 0;
		
        kickerId = setInterval(kickUser,interval || 1000);
    }
    function stopKicking(){
        if(kickerId != null){
            clearInterval(kickerId);
            kickerId = null;
        }else{
            console.log("nothing to stop")
        }

    }

    function kickUser(userName){
	
		_this.userName = userName || _this.userName;
		
        _this.counter++;
        var msg=prepareMsg();


        var settings = {
            url: "https://docusafe.hipchat.com/v2/user/"+_this.userName+"/message?auth_token="+auth_token,
            contentType :'application/json',
            dataType:"json",
            type: "POST",
            data: JSON.stringify({
                "message":msg,
                "message_format":"text",
                "notify" : "true"
            })
        };


        $.ajax(settings).always(function(resp){window.resp = resp;console.log(resp);});
    }

    function prepareMsg(){
        var result=_this.message;
        if(_this.multiple){
            for(var i = 1; i < _this.counter; i++){
                result += _this.message;
            }
        }
        return result;
    }

    window.hipchatKicker = _this;
})();

