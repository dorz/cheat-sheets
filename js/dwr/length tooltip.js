$j(document).ready(function(){
	$j('.txt').each(function(){
		var cn = $j(this).attr('id');
		if(cn == null){
			return;
		}
		
		if(cn.startsWith('DWR_')){
			if(fieldsManager.fields[cn] == null){
				return;
			}
			var length = fieldsManager.fields[cn].jField.length;
			if(length != null){
				$j(this).attr('title',"Maksymalna ilość znaków " + length);
			}
		}else if($j(this).parent().hasClass('DICTIONARY_FIELD')){
			fullCn = cn;
			cn = $j(this).attr('name');
			
			var dicCn = fullCn.replace('_'+$j(this).attr('name'),'');
			dicCn = 'DWR_' + dicCn;
			
			if(fieldsManager.fields[dicCn] == null){
				return;
			}
			var dfm = fieldsManager.fields[dicCn].dictionaryFieldsManager;
			if(dfm.fields[cn] == null){
				return;
			}
			var length = dfm.fields[cn].jField.length;
			if(length != null){
				$j(this).attr('title',"Maksymalna ilość znaków " + length);
			}
		}
	});
});