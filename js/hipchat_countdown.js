(function (){
    var auth_token = ;

    var _this = this;
    var _kickerId = null;
	var _state = "INIT";
	var _endCounter = 0;
	var _replacingText = "?"
	
    _this.message = "?";
    _this.counter = 0;
    _this.endCounterLimit = 0;
	_this.interval = 1000;

    _this._restart = restart;
    _this._start = start;
    _this._stop = stop;
	_this._oneKick = sendMessage;

    function restart(userName, message, counter, endCounterLimit, interval){
		stop();
		start(userName, message, counter, endCounterLimit, interval);
	}
		
    function start(userName, message, counter, endCounterLimit, interval){
        if(_kickerId != null){
            throw "can kick one user at once"
        }
        if(userName == null){
            throw "userName could not be null"
        }
		
		reset();
		
        _this.userName = userName;
        _this.message = message || _this.message;
		_this.counter = counter || _this.counter;
		_this.interval = !interval || interval < 500 ? _this.interval : interval;
		_this.endCounterLimit = endCounterLimit || _this.endCounterLimit;
		
        _kickerId = setInterval(send, _this.interval);
    }
	
	function reset(){
		_this.message = "?";
		_this.counter = 0;
		_this.endCounterLimit = 0
		_this.interval = 1000;
		_state = "INIT";
		_replacingText = "?"
		_endCounter = 0;
		_kickerId = null;
	}
	
    function stop(){
        if(_kickerId != null){
            clearInterval(_kickerId);
			reset();
        }else{
            console.log("nothing to stop")
        }
    }
	
	function send(){
		if (_state === "INIT"){
			_state = "COUNTDOWN";
			sendInitMsg();
		} else {
			countdown();
			if (_state === "COUNTDOWN"){
				sendUpdate(_this.counter);
			} else {
				_endCounter++;
				if (_endCounter <= _this.endCounterLimit){
					sendUpdate(_endCounter%2==0 ? "ZERO" : "zero");
				} else {
					console.log('STOP HIPCHAT-COUNTDOWN');
					_stop();
				}
			}
		}
	}
	
    function sendInitMsg(){
		var newReplacedText = _this.counter;
		var message = _this.message.replace(_replacingText, _this.counter);
		_replacingText = newReplacedText;
		sendMessage(message);
    }
	function countdown(){
		if (_this.counter <= 0){
			_state = "END";
			return;
		}
		_this.counter--;
	}
	function sendUpdate(replacement){
		var message = "s/" + _replacingText + "/" + replacement;
		_replacingText = replacement;
		sendMessage(message);
	}

    function sendMessage(message){
        var settings = {
            url: "https://docusafe.hipchat.com/v2/user/"+userName+"/message?auth_token="+auth_token,
            contentType :'application/json',
            dataType:"json",
            type: "POST",
            data: JSON.stringify({
                "message":message,
                "message_format":"text",
                "notify" : "true"
            })
        };

        $j.ajax(settings).always(function(resp){
				window.resp = resp; 
				console.log('RESP:',resp,'FOR MESSAGE',message);
			}
		);
    }

    window.hipchatKicker = _this;
})();

