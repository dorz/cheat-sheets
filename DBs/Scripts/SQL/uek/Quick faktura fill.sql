declare @idToFill numeric(19,0) =  30617   

declare @docToCopy numeric(19,0) = 30612



declare @status numeric(18,0) = (select status FROM [UEK_TEST].[dbo].[dsg_uek_faktura_zakupowa] where document_id = @idToFill)

delete from [dbo].[dsg_uek_faktura_zakupowa] where document_id = @idToFill


insert into [dbo].[dsg_uek_faktura_zakupowa] 
SELECT @idToFill as document_id
      ,@status as status
      ,[OSTATNIA_FAKTURA_BOOL]
      ,[BRAK_KONTRAHENTA]
      ,[CONTRACTOR]
      ,[NR_FAKTURY]
      ,[TYP_FAKTURY]
      ,[DATA_WYSTAWIENIA]
      ,[DATA_WPLYWU]
      ,[SPOSOB_PLATNOSCI]
      ,[SPOSOB_ZWROTU]
      ,[KWOTA_NETTO]
      ,[KWOTA_VAT]
      ,[KWOTA_BRUTTO]
      ,[KWOTA_WYPLATY]
      ,[NKUP]
      ,[DATA_ZAMOWIENIA]
      ,[TRYB_ZAMOWIENIA]
      ,[SZCZEGOLOWY_TRYB]
      ,[NUMER_ZAMOWIENIA]
      ,[NUMER_BZP]
      ,[WALUTA]
      ,[KOD_USLUGI]
      ,[KURS]
      ,[OPIS_FAKTURY]
      ,[OPIS_MERYTORYCZNY]
      ,[WAS_PROFORMA]
      ,[PROFORMA]
      ,[WNIOSEK_O_REZERWACJE_BOOL]
      ,[WNIOSEK_O_REZERWACJE]
      ,[OPLATA_ADMINISTRACYJNA]
      ,[UMOWA]
      ,[OPIS_OPLATY]
      ,[NUMER_UMOWY]
      ,[CZY_MAGAZYNOWA]
      ,[PRODUCT]
      ,[PRODUCTION_ORDER]
      ,[MPK]
      ,[PRZED_WYKONANIEM]
      ,[DATA_WYKONANIA]
      ,[RODZAJ_ZAMOWIENIA]
      ,[QUANTITY]
      ,[UNIT_OF_MEASURE]
      ,[BUDGET_KIND]
      ,[SOURCE]
      ,[BUDGET]
      ,[BUDGET_KO]
      ,[POSITION]
      ,[FINANCIAL_TASK]
      ,[PROJECT_BUDGET]
      ,[CONTRACT]
      ,[CONTRACT_STAGE]
      ,[FUND]
      ,[WS_KOD]
      ,[WS_OBSZAR]
      ,[WS_KWOTA]
      ,[ENTRY_ID]
      ,[DSD_REPO89_PRZEDSIEWZIECIA_DYDAKTYCZNE]
      ,[DSD_REPO95_RODZAJ_STUDIOW]
      ,[DSD_REPO77_RODZAJ_PRZEDSIEWZIECIA]
      ,[DSD_REPO88_BUDYNKI]
      ,[DSD_REPO97_DS]
      ,[DSD_REPO98_DOTACJA]
      ,[DSD_REPO119_KPZF]
      ,[ERP_STATUS]
      ,[ERP_DOCUMENT_IDM]
      ,[PURCHASE_PLAN]
      ,[PURCHASE_PLAN_KIND]
      ,[DELEGACJA]
  FROM [UEK_TEST].[dbo].[dsg_uek_faktura_zakupowa] where document_id = @docToCopy

  

  delete from [dbo].[dsg_uek_faktura_zakupowa_multiple_value] where document_id = @idToFill

  insert into [dbo].[dsg_uek_faktura_zakupowa_multiple_value] 
  SELECT @idToFill as document_id
      ,[FIELD_CN]
      ,[FIELD_VAL]
  FROM [UEK_TEST].[dbo].[dsg_uek_faktura_zakupowa_multiple_value] where document_id = @docToCopy

