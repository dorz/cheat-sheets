---------------ACT_ID_MEMBERSHIP-------------
CREATE VIEW ACT_ID_MEMBERSHIP AS
select u.NAME as USER_ID_,
		d.GUID as GROUP_ID_
from DS_USER as u
	join DS_USER_TO_DIVISION as ud on u.ID = ud.USER_ID
	join DS_DIVISION as d on ud.DIVISION_ID = d.ID 
where d.DIVISIONTYPE = 'division'

UNION

SELECT 'admin' as USER_ID_,
		'admin' AS GROUP_ID_
		
GO




---------------ACT_ID_GROUP ---------------------
CREATE VIEW ACT_ID_GROUP AS
select	cast(GUID as nvarchar(64)) as  ID_,
		cast(1 as int) AS REV_,
		cast(NAME as nvarchar(255))as Name_,
		cast('assignment' as nvarchar(255)) as TYPE_
from DS_DIVISION
where DIVISIONTYPE = 'division'

UNION

select	cast('admin' as nvarchar(64)) as  ID_,
		cast(1 as int) AS REV_,
		cast('Admin' as nvarchar(255)) as Name_,
		cast('security-role' as nvarchar(255)) as TYPE_

--select * from ACT_ID_GROUP_ WHERE TYPE_ != 'assignment'

GO


--------------------ACT_ID_USER------------
CREATE VIEW ACT_ID_USER AS
Select NAME as ID_,
	2 as REV_,
	FIRSTNAME AS FIRST_,
	LASTNAME AS LAST_,
	'' AS EMAIL_,
	IIF(ID  = 1, 'a', 'bardzotajnesuperhaslo'+CAST(id AS VARCHAR(5))) as PWD_,
	----'a' as PWD_,
	CASE  
		WHEN ID = 1 THEN 7
		WHEN ID % 2 = 0 THEN 18
		ELSE 22
	END AS PICTURE_ID_
	--null as PICTURE_ID_
 from DS_USER

