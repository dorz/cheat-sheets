WITH CTE
AS(
  SELECT h.document_id, h.ctime, status, processname,
  count(document_id) over(partition by document_id ORDER BY document_id, h.ctime) AS LICZ, 
  LAST_VALUE(status) over(partition by document_id ORDER BY document_id) AS last_status
  FROM dso_document_asgn_history h join ds_document doc on doc.id = h.document_id 
  where   doc.dockind_id = 8 and
  (select count(1) from dsw_jbpm_tasklist t where t.document_id = h.document_id) < 1  
), 
statusy_posrenie as(
select * from cte where last_status != 'Faktura odrzucona' and last_status != 'Uzupełnienie rodzaju faktury' 
) 
select distinct document_id,last_status from statusy_posrenie