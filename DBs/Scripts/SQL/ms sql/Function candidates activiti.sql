declare @docId bigint = 20408

declare @database sysname = 'UEK_test'

declare @docIdName varchar(max) = 'docId'
declare @sqlQuery varchar(max) = 
';WITH execution AS
(
	SELECT EXECUTION_ID_ FROM ACT_RU_VARIABLE WHERE NAME_ = '+@docIdName+' and LONG_ = '+ convert(@docId,'varchar(50)') +'
), task AS
(
	SELECT ID_ FROM ACT_RU_TASK WHERE EXECUTION_ID_ = (SELECT * FROM execution)
)
SELECT TYPE_,USER_ID_,TASK_ID_ FROM ACT_RU_IDENTITYLINK WHERE TASK_ID_ = (SELECT * FROM task)'


exec(@sqlQuery)