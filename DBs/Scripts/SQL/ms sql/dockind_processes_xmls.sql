Create View ds_dockind_processes_xmls_view as
with ds_dck_xml as(
    SELECT CONVERT(XML, CONVERT(VARBINARY(MAX), "content")) AS dockind,
      cn,
      tablename
    FROM DS_DOCUMENT_KIND
), dockind_processes as (
    SELECT
      cn AS dockind_cn,
      process_cn
    FROM ds_dck_xml
      CROSS APPLY
      (
        SELECT t.k.value('.', 'varchar(256)') AS process_cn
        FROM dockind.nodes('doctype/processes/spring-process-definition') AS t(k)
      ) AS A
), procdef as (
    SELECT
      MAX(VERSION_)OVER (PARTITION BY KEY_) as last_version,
      KEY_,
      VERSION_,
      DEPLOYMENT_ID_,
      RESOURCE_NAME_
    FROM ACT_RE_PROCDEF
), procdefxml as (
    SELECT
      dockind_cn,
      process_cn,
      convert(XML, replace(convert(NVARCHAR(MAX), CONVERT(VARCHAR(MAX), CONVERT(VARBINARY(MAX), BYTES_))), 'UTF-8', 'UTF-16')) AS proc_xml
    FROM procdef pd
      JOIN dockind_processes dkp ON pd.last_version = pd.VERSION_ AND pd.KEY_ = dkp.process_cn
      JOIN ACT_GE_BYTEARRAY gb ON pd.DEPLOYMENT_ID_ = gb.DEPLOYMENT_ID_ AND gb.NAME_ = RESOURCE_NAME_
)
select dockind_cn, q as procxml
from procdefxml pdx
  CROSS APPLY (
     select
       t.k.query('.') as q
     from pdx.proc_xml.nodes('.') as t(k)


) as A

