CREATE FUNCTION documentCandidates( @docId bigint)

RETURNS TABLE
AS

RETURN (	WITH	processInstance AS
					(
						SELECT PROC_INST_ID_ FROM ACT_RU_VARIABLE WHERE NAME_ = 'docId' and LONG_ = @docId
					), task AS
					(
						SELECT ID_ , PROC_DEF_ID_, NAME_ FROM ACT_RU_TASK WHERE PROC_INST_ID_ in (SELECT * FROM processInstance)
					)
			SELECT NAME_ AS TASK, 
					TYPE_ AS TYP ,
					USER_ID_ AS UserName, 
					GROUP_ID_ AS "Group",
					task.PROC_DEF_ID_ AS ProcesDefinition 
			FROM ACT_RU_IDENTITYLINK AS i JOIN task ON i.TASK_ID_ = task.ID_
		)



