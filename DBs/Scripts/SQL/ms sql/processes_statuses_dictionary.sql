REQUIRES dockind_processes_xmls.sql

CREATE VIEW ds_processes_statuses_dictionary as
with XMLNAMESPACES ( 
	'http://www.w3.org/2001/XMLSchema-instance' as "xsi",
    'http://www.w3.org/2001/XMLSchema' as "xsd",
    'http://activiti.org/bpmn' as "activiti",
    'http://www.omg.org/spec/BPMN/20100524/DI' as "bpmndi",
    'http://www.omg.org/spec/DD/20100524/DC' as "omgdc",
    'http://www.omg.org/spec/DD/20100524/DI' as "omgdi",
    DEFAULT 'http://www.omg.org/spec/BPMN/20100524/MODEL'
), tasks as(
select dockind_cn, task_id, ext 
from ds_dockind_processes_xmls_view
cross apply (
	select 
	k.value('@id','varchar(256)') as task_id,
	k.query('./extensionElements') as ext
	from procxml.nodes('definitions/process/userTask') as t(k)
) as A
), listeners_expressions as (
select dockind_cn, task_id, convert(varchar(512),listener) as listener 
from tasks
cross apply (
	select 
	k.value('@expression', 'varchar(512)') as listener
	from ext.nodes('extensionElements/activiti:taskListener') as t(k)
	
) as A
)
select dockind_cn, task_id, 
SUBSTRING(listener,charindex(',',listener) + 2,charindex(')', listener) - charindex(',',listener) - 2) as status_id 
from listeners_expressions
where listener like '%setStatus%'

