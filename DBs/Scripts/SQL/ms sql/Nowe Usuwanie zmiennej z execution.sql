--USUWANIE ZMIENNEJ KOREKCJI
DECLARE @document_id numeric(18,0) = 34086		--TU PODAĆ NUMER DOKUMENTU
DECLARE @var_name varchar(255) = 'correction'	--TU PODAĆ NAZWĘ ZMIENNEJ



---------TU WYBIERANA JEST ZMIENNA Z BAZY ---------------
;with ex as (
		SELECT execution_ FROM JBPM4_VARIABLE where long_value_=@document_id
), variables as(
		SELECT DBID_
		--, KEY_, v.EXECUTION_ 
		FROM JBPM4_VARIABLE 
		where execution_ in (select execution_ from ex) and KEY_ = @var_name 
),cor as(
	SELECT *
	FROM JBPM4_VARIABLE
	where DBID_ in (select dbid_ from variables)
)


select * from cor

----------Jeżeli jesteś pewny że to ta zmienna to, komentujesz "select * from cor", a odkomentowujesz poniższe delete------------------

--delete from JBPM4_VARIABLE where dbid_ in (select DBID_ from cor) 

