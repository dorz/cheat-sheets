CREATE PROCEDURE KickJob
    @docId BIGINT
AS 
;with jobToKick AS 
(
  SELECT j.*
  FROM ACT_RU_JOB j
  JOIN ACT_RU_EXECUTION e ON j.EXECUTION_ID_ = e.ID_
  JOIN ACT_RU_VARIABLE v ON e.ID_ = v.EXECUTION_ID_ WHERE name_ = 'docId' AND LONG_ = @docId
)
update jobToKick set exception_stack_id_ = null, exception_msg_ = null, RETRIES_ = 3

GO


--HOW TO USE:
--exec KickJob @docId = 43067