with RECURSIVE divisions as (
  SELECT id, guid, name, name::TEXT as path  FROM ds_division where parent_id is null and divisiontype='division'
  UNION
  select dsd.id, dsd.guid, dsd.name, concat(d.path, ' / ', dsd.name)
  FROM ds_division dsd join divisions d on d.id = dsd.parent_id
  where divisiontype = 'division'
)
select * from divisions;