alter table ZORO_LOSS_EVENT add  text_data clob;

update ZORO_LOSS_EVENT set text_data = loss_description || ' ' || recognition_description  || ' ' || clearing_activities  || ' ' || loss_example_Description;


CREATE INDEX AKT_TEXT_DATA_IDX ON ZORO_LOSS_EVENT

  (

    text_data

  )

  INDEXTYPE IS "CTXSYS"."CONTEXT" ;
  
  BEGIN


-- Job defined entirely by the CREATE JOB procedure.

DBMS_SCHEDULER.DROP_JOB(JOB_NAME => 'sync_index');

DBMS_SCHEDULER.create_job (

job_name => 'sync_index',

job_type => 'PLSQL_BLOCK',

job_action => 'begin update ZORO_LOSS_EVENT set text_data = loss_description || '' '' || recognition_description  || '' '' || clearing_activities  || '' '' || loss_example_Description; ctx_ddl.sync_index(''AKT_TEXT_DATA_IDX''); end;', start_date => SYSTIMESTAMP, repeat_interval => 'freq=minutely', end_date => NULL, enabled => TRUE, comments => 'updating text index'); END;

commit;


--Przyk�ad query:
-- select score(1),* from ZORO_LOSS_EVENT WHERE  CONTAINS(text_data, 'uwagi  test', 1) > 0;